# EKIFoundation

EKIFoundation 封装常用的基础库，数据模型。算法，数据结构

- [ ] weakSelf strongSelf宏
- [ ] 自释放 Timer
- [ ] 线程安全队列
- [ ] 线程安全字典
- [ ] 线程安全优先队列